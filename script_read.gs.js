var sheet_id = "1KZQzlEifd9BOzCJnMOIfkDKGvdfi3ye72FguM5ozHaE";
var ss = SpreadsheetApp.openById(sheet_id);
var sheet = ss.getSheetByName('Agendados');

function doGet(e){
  var read = e.parameter.read;
  
  var val = e.parameter.flag;
  if (e.parameter.flag !== undefined){
    var range = sheet.getRange('F2');
    range.setValue(val);
  }

  if (read !== undefined){
    var text = ContentService.createTextOutput(sheet.getRange(2,1,4,3).getValues());
    text.append(",");
    text.append(sheet.getRange(3,6,6,1).getValues());
    return text;
  }
}


function doPost (e) {
  const lock = LockService.getScriptLock()
  lock.tryLock(10000)

  try {
    const headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0]
    const nextRow = sheet.getLastRow() + 1

    var newRow = [];

    for (i in headers){
      newRow.push(e.parameter[headers[i]]);
    }

    sheet.getRange(nextRow, 1, 1, newRow.length).setValues([newRow])

    return ContentService
      .createTextOutput(JSON.stringify({ 'result': 'success', 'row': nextRow }))
      .setMimeType(ContentService.MimeType.JSON)
  }

  catch (e) {
    return ContentService
      .createTextOutput(JSON.stringify({ 'result': 'error', 'error': e }))
      .setMimeType(ContentService.MimeType.JSON)
  }

  finally {
    lock.releaseLock()
  }
}
