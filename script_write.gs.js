var sheet_id = "1r3uC1D_jjPuIifYVI7_C2nCU5DkzaCCdgCjLRvHv-wY";
var ss = SpreadsheetApp.openById(sheet_id);
var sheet = ss.getSheetByName('Historico');

function doGet(e){
  var hora_atual = ContentService.createTextOutput(e.parameter.hora_atual);
  var min_atual = ContentService.createTextOutput(e.parameter.min_atual);
  var peso = Number(e.parameter.peso);
  var dia = ContentService.createTextOutput(e.parameter.dia);
  var mes = ContentService.createTextOutput(e.parameter.mes);
  var ano = ContentService.createTextOutput(e.parameter.ano);
  
  hora_atual.append(":");
  hora_atual.append(min_atual.getContent());

  dia.append("/");
  dia.append(mes.getContent());
  dia.append("/");
  dia.append(ano.getContent());
  sheet.appendRow([hora_atual.getContent(), peso, dia.getContent()]);  
}