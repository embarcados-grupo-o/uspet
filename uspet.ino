/*
    INCLUDES
*/
//#include <FreeRTOS.h> por algum motivo funciona sem
//#include <queue.h>
#include <HTTPClient.h>
#include <ESP32Servo.h>
#include <WiFi.h>
#include <Arduino.h>
#include "HX711.h"
#include "soc/rtc.h"

/*
    PINOS
*/
#define LOADCELL_DOUT_PIN  16
#define LOADCELL_SCK_PIN  4
#define PRESENCA_RAC  17
#define PRESENCA_CAO  5
#define SERVOPIN 13

/*
    DEFINICOES DO SISTEMA
*/
#define NUM_ESTADOS 4
#define NUM_EVENTOS 8

// ESTADOS
#define ESPERA         0
#define V_FECHADA      1
#define V_ABERTA_BOTAO 2
#define V_ABERTA_AUTO  3

// EVENTOS
#define NENHUM_EVENTO          -1
#define HORARIO_PROGRAMADO     0
#define SENSOR_PESO            1
#define BOTAO_PRESSIONADO      2
#define BOTAO_N_PRESSIONADO    3
#define PESO_CTE               4
#define CONF_ABASTECIMENTO     5
#define SENSOR_CACHORRO        6
#define SENSOR_N_CACHORRO      7

// ACOES
#define NENHUMA_ACAO                 -1
#define ABRE_RACAO                    0
#define FECHA_RACAO                   1
#define FECHA_RACAO_MANDA_DADOS_ESP   2
#define ZERA_DADOS_ESP                3
#define FECHA_RACAO_ALARME            4


// MAQUINA DE ESTADOS
int codigoEvento = NENHUM_EVENTO;
int eventoInterno = NENHUM_EVENTO;
int estado = V_FECHADA;
int codigoAcao;


int proximo_estado_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS] = {
    {V_FECHADA, ESPERA, ESPERA, ESPERA, ESPERA, V_ABERTA_AUTO, ESPERA, V_ABERTA_AUTO},
    {V_ABERTA_AUTO, V_FECHADA, V_ABERTA_BOTAO, V_FECHADA, V_FECHADA, V_FECHADA, V_FECHADA, V_FECHADA},
    {V_ABERTA_BOTAO, V_ABERTA_BOTAO, V_ABERTA_BOTAO, V_FECHADA, V_ABERTA_BOTAO, V_ABERTA_BOTAO, V_ABERTA_BOTAO, V_ABERTA_BOTAO},
    {V_ABERTA_AUTO, V_FECHADA, V_ABERTA_AUTO, V_ABERTA_AUTO, ESPERA, V_ABERTA_AUTO, ESPERA, V_ABERTA_AUTO}
};

int acao_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS] = {
    {ZERA_DADOS_ESP, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, ABRE_RACAO, NENHUMA_ACAO, ABRE_RACAO},
    {ABRE_RACAO, NENHUMA_ACAO, ABRE_RACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO},
    {NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, FECHA_RACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO},
    {NENHUMA_ACAO, FECHA_RACAO, NENHUMA_ACAO, NENHUMA_ACAO, FECHA_RACAO_ALARME, NENHUMA_ACAO, FECHA_RACAO_MANDA_DADOS_ESP, NENHUMA_ACAO}
};

/*
    FreeRTOS
*/
void taskMaqEstados(void *pvParameters);
void taskObterEvento(void *pvParameters);
QueueHandle_t xQueue;
TaskHandle_t xTaskMaqEstados;
TaskHandle_t xTaskObterEvento;

/*
    Servo
*/
Servo myservo; 

/*
    Funcoes balanca
*/
HX711 scale;
int med_igual = 0;  //contador de vezes que o valor de peso é igual ao anterior
int med_ant = 0;    //peso anterior
int med_inic = 0;   //medicao inicial de quando abre a porta
int flag_balanca_cte = 0;   //flag para peso constante
int flag_balanca = 0;       //flag para saber se chegou no peso esperado ou no peso maximo
int peso_abast = 0;         //peso para abastecer
int peso_max = 1000;        //peso maximo que cabe na balanca
int peso_espera = 0;        //peso salvo pelo estado espera (o quanto falta colocar)
int alarme_peso_cte = 0;    //flag para mandar aviso pro app

void bal_init(){
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN); //Inicia a balanca
  scale.set_scale(-471.497); //Fator de calibracao
  scale.tare(); //Zera a balanca
}

void bal_update(){//
  int med=scale.get_units(5);
  if ((((med-med_inic+peso_espera)>=peso_abast)||((med-med_inic+peso_espera)>=peso_max))&&(estado==V_ABERTA_AUTO)){
    flag_balanca = 1;
  }
}

void bal_update_cte(){
  if ((abs(med_ant-scale.get_units())<=1)&&(estado==V_ABERTA_AUTO)){//checa se o valor atual é igual ao anterior
    med_igual=med_igual+1;  
  } else {
    med_igual=0;
  }
  if(med_igual>=10){//se for igual em 10 medidas fala que a  balanca não se altera
    flag_balanca_cte=1;
  }
}

/*
    Funcoes sensores de presenca
*/
int flag_sensor_cachorro = 0;        // sensor da presença do cachorro
int flag_sensor_racao = 0;           // sensor da presença de ração
void pres_init(){
  pinMode(PRESENCA_RAC, INPUT); 
  pinMode(PRESENCA_CAO, INPUT); 
}
void pres_cao_update(){
  flag_sensor_cachorro = !digitalRead(PRESENCA_CAO);
}
void pres_rac_update(){
  flag_sensor_racao = digitalRead(PRESENCA_RAC);
}


/*
    Wifi e Google Sheets
*/
int botao_abrir = 0;            // botão de abrir a ração (0 - fecha e 1 - abre)
int botao_reabastecer = 0;      // botão para confirmar que a ração foi reabastecida

int hora_agendada;
int min_agendado;
int flag_horario_agendado = 0;
int hora_atual = 0;
int min_atual = 0;
int peso_atual = 0;
int dia;
int mes;
int ano;

const char* ssid = "Yudi";
const char* password = "12345678";
WiFiServer server(80);

int flag_btn_app=0;
String header;

// Google script ID and required credentials
String GOOGLE_SCRIPT_ID_READ = "AKfycbzQDnB6ZtzHW8BNikGW3qGAZZ1VGJzhcsf3BBaaI316OCkhrpzr6DY6oCTrXVODk05uCw";    // change Gscript ID
String GOOGLE_SCRIPT_ID_WRITE = "AKfycbxYrsRko4zOe7LUGiIYszrZeqLzAZLr7c-kG-iuWP4jdndkuQUDyqAKnhbhRR7ZsJu-bQ";

void conecta_wifi(){
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void sheets_update(){
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    String url = "https://script.google.com/macros/s/" + GOOGLE_SCRIPT_ID_READ + "/exec?read&value=1";
    Serial.println("Making a request");
    http.begin(url.c_str()); //Specify the URL and certificate
    http.setFollowRedirects(HTTPC_STRICT_FOLLOW_REDIRECTS);
    int httpCode = http.GET();
    
    String tabela_horarios;
    int parsedArray[20];                                                     
    int nb = 0;

    int flag_salvar_bd; // flag que indica quando é hora cheia e precisa salvar o peso no BD

    if (httpCode > 0) { //Check for the returning code
      tabela_horarios = http.getString();

      int n = tabela_horarios.length();
      
      char p[n+1];
      int num = 0;
      strcpy(p, tabela_horarios.c_str());

      for(int i = 0; i < n; i++){
        if(p[i] == ','){
          parsedArray[nb++] = num;
          num = 0;
        }
        else
          num = 10*num + p[i] - 48;
      }
      parsedArray[nb++] = num;

      nb -= 5;

      int horas[5], minutos[5], pesos[5];
      int peso;
      
      for(int i = 0; i < nb/3; i++){
        horas[i] = parsedArray[3*i];
        minutos[i] = parsedArray[3*i+1];
        pesos[i] = parsedArray[3*i+2];
      }
      
      hora_atual = parsedArray[nb];
      min_atual = parsedArray[nb+1];
      dia = parsedArray[nb+2];
      mes = parsedArray[nb+3];
      ano = parsedArray[nb+4];

      Serial.print("Agora - ");
      Serial.print(hora_atual);
      Serial.print(":");
      Serial.println(min_atual);
      Serial.println("");
      
      for(int i = 0; i < nb/3; i++){
       if (horas[i] == hora_atual && minutos[i] == min_atual) {
          peso = pesos[i];
          Serial.println("Dando comida!");
          Serial.print(peso);
          Serial.println("g");
       }

       if (min_atual == 0) {
        flag_salvar_bd = 1;
       }
      }
    }
    else {
      Serial.println("Error on HTTP request");
    }
    http.end();

    if (flag_salvar_bd == 1) {
        int peso_atual = scale.get_units(5); //pega o peso da balança

        HTTPClient http;
        String url = "https://script.google.com/macros/s/" + GOOGLE_SCRIPT_ID_WRITE + "/exec?hora_atual=" + hora_atual + "&min_atual=" + min_atual + "&peso=" + peso_atual + "&dia=" + dia + "&mes=" + mes + "&ano=" + ano;
        Serial.print("POST data to spreadsheet:");
        http.begin(url.c_str()); //Specify the URL and certificate
        http.setFollowRedirects(HTTPC_STRICT_FOLLOW_REDIRECTS);
        int httpCode = http.GET();
        Serial.print("HTTP Status Code: ");
        Serial.println(httpCode);
        http.end();
        flag_salvar_bd=0;
    }
  }
}

void site_update(){
  WiFiClient client = server.available();
  if (client) {                             // Se houver coneccao externa
    Serial.println("New Client.");         
    String currentLine = "";                
    while (client.connected()) {  // Enquanto o cliente estiver conectado
      if (client.available()) {             // Se tiver dados para ler
        char c = client.read();             // Le o byte
        Serial.write(c);                    
        header += c; //adiciona o byte na linha
        if (c == '\n') {                    // se acabou a linha
          if (currentLine.length() == 0) {//se acabouo o request manda a resposta
            //manda a resposta
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            int bot=0;//ve se é uma pagina de botao
            //checa o site 
            if (header.indexOf("GET /aberto") >= 0) {
              Serial.println("Aberto");
              botao_abrir = 1;
              bot = 1;
            } else if (header.indexOf("GET /fechado") >= 0) {
              Serial.println("Fechado");
              botao_abrir = 0;
              bot = 1;
            }  else if (header.indexOf("GET /reab") >= 0) {
              Serial.println("Reabastecer");
              botao_reabastecer = 1;
            } 
            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta charset=\"UTF-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><title>USPet</title></head>");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>h1 {text-align: center;} .form {margin: 10% 5%;} .open {padding-left: 2%; font-size: 30px;}");
            client.println(".button {background-color: #555555;border: none; color:white;padding: 12px 15px;margin-top: 5%;text-align: center;text-decoration: none;");
            client.println("display: inline-block;font-size: 15px; border-radius: 15px;box-shadow: 6px 5px 3px rgba(0,0,0,0.2);cursor: pointer;}");
            client.println(".button:hover {background-color: #252525}.button:active {background-color: #a7a7a7;color: black;box-shadow: 6px 4px 3px rgba(0,0,0,0.2);transform: translateY(4px);}");
            client.println(".switch-div {display: flex;flex-direction: row;justify-content: left;}.switch {position: relative;display: inline-block;width: 60px;height: 34px;}");
            client.println(".switch input {opacity: 0;width: 0;height: 0;}.slider {position: absolute;cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #ccc;-webkit-transition: .4s;transition: .4s;}");
            client.println(".slider:before {position: absolute;content: "";height: 26px;width: 26px;left: 4px;bottom: 4px;background-color: white;-webkit-transition: .4s;transition: .4s;}");
            client.println("input:checked + .slider {background-color: #2196F3;}input:focus + .slider {box-shadow: 0 0 1px #2196F3;}input:checked + .slider:before {-webkit-transform: translateX(26px);-ms-transform: translateX(26px);transform: translateX(26px);}");
            client.println(".slider.round {border-radius: 34px;}.slider.round:before {border-radius: 50%;} </style>");
            client.println(".botao-hist {text-align: center;}.aviso {margin: 5% 5%;}</style>");
            client.println("</head>");
            
            // Web Page Heading
            client.println("<body><div class=\"title\"><h1>USPet</h1></div>");
            if(flag_balanca_cte){
              client.println("<div class=\"aviso\"><hr><div class=\"botao\"><h2>AVISO!</h2><p>Acabou a ração!</p><a href=\"/reab\"><button type=\"submit\" class=\"button\">Reabastecido</button></a> </div></div>");
            } else if (flag_sensor_racao){
              client.println("<div class=\"aviso\"><hr><div class=\"botao\"><h2>AVISO!</h2><p>Racao acabando!</p></div></div>");
            }
            if(botao_abrir==1){
              client.println("<hr><div class=\"form\"><a href=\"/fechado\"><button type=\"submit\" class=\"button\">Fechar</button></a></div>");
            }else {
              client.println("<hr><div class=\"form\"><a href=\"/aberto\"><button type=\"submit\" class=\"button\">Abrir</button></a></div>");
            }
            client.println("<hr><form method=\"POST\" action=\"https://script.google.com/macros/s/AKfycbzBHyN7U-PWnJMwHaKugsh93zQ860LssQTN_twURez_NZVornEJxJc_022RDFEmT2ehZg/exec\"><div class=\"form\"><label>Hora</label><input name=\"Hora\" type=\"number\" required><br><br><label>Minuto</label><input name=\"Minuto\" type=\"number\" required><br><br><label>Peso</label><input name=\"Peso\" type=\"number\" required> g <br><br><button type=\"submit\">Enviar</button></div></form>");
            client.println("<hr><div class=\"botao-hist\"><div class=\"botao\"><a href=\"graph.html\"><button type=\"submit\" class=\"button\">Histórico do consumo</button> </a></div></div></body></html>");

            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

/*
    Funcoes da maquina de estado
*/
int obterAcao(int estado, int codigoEvento) {
  return acao_matrizTransicaoEstados[estado][codigoEvento];
}

int obterProximoEstado(int estado, int codigoEvento) {
  return proximo_estado_matrizTransicaoEstados[estado][codigoEvento];
}


void executarAcao(int codigoAcao) {
    if (codigoAcao == NENHUMA_ACAO)
        return;

    switch(codigoAcao)
    {
        case ABRE_RACAO:
            printf("Abre ração\n");
            med_inic = scale.get_units(5);
            myservo.write(0);
            break;
        case FECHA_RACAO:
            printf("Fecha ração\n");
            flag_balanca = 0;//zera a flag
            myservo.write(90);
            break;
        case FECHA_RACAO_MANDA_DADOS_ESP:
            printf("Fecha ração e manda quantidade para ESP32\n");
            myservo.write(90);
            peso_espera = scale.get_units(10)-med_inic;
            break;
        case ZERA_DADOS_ESP:
            printf("Zera o dado salvo no ESP32\n");
            peso_espera = 0;
            break;
        case FECHA_RACAO_ALARME:
            printf("Fecha ração, manda alarme que a ração está acabando e salva a qtde no ESP32\n");
            myservo.write(90);
            alarme_peso_cte = 1;
            flag_balanca_cte = 0;//zera a flag
            peso_espera = scale.get_units(10)-med_inic; // não vai ser usado até a balança dar certo
            break;
    }
}

/*
    Tasks RTOS
*/
void taskMaqEstados(void *pvParameters) {
  int codigoEvento;
  BaseType_t xStatus;

  for( ;; ) {
    if( xQueueReceive( xQueue, &codigoEvento, portMAX_DELAY ) == pdPASS ) {
      if (codigoEvento != NENHUM_EVENTO)
      {
        codigoAcao = obterAcao(estado, codigoEvento);
        estado = obterProximoEstado(estado, codigoEvento);
        executarAcao(codigoAcao);
        Serial.print("Estado: ");
        Serial.print(estado);
        Serial.print("Evento: ");
        Serial.print(codigoEvento);
        Serial.print("Acao: ");
        Serial.println(codigoAcao);
      }
    }
    else {
      Serial.println("Erro ao receber evento da fila");
    }
  }
}

void taskObterEvento(void *pvParameters) {
  int codigoEvento;
  BaseType_t xStatus;

  for( ;; ) {
    codigoEvento = NENHUM_EVENTO;

    //update dos sensores
    bal_update_cte();
    bal_update();
    pres_rac_update();
    pres_cao_update();
    site_update();
    sheets_update();
    
    if (flag_horario_agendado == 1 && (estado==ESPERA || estado==V_FECHADA)) { // horário programado ou passou 1 dia com o cachorro sem comida :(
        codigoEvento = HORARIO_PROGRAMADO; 
        peso_espera = 0;
        flag_horario_agendado = 0;
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (flag_balanca && estado == V_ABERTA_AUTO) {
        codigoEvento = SENSOR_PESO; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (estado == V_FECHADA && botao_abrir == 1) {
        codigoEvento = BOTAO_PRESSIONADO; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (estado == V_ABERTA_BOTAO && botao_abrir == 0) {
        codigoEvento = BOTAO_N_PRESSIONADO; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (flag_balanca_cte && estado == V_ABERTA_AUTO) { //sem a balança isso nunca vai acontecer
        codigoEvento = PESO_CTE; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (estado == ESPERA && botao_reabastecer == 1) {
        alarme_peso_cte = 0;
        botao_reabastecer = 0;
        codigoEvento = CONF_ABASTECIMENTO; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (estado == V_ABERTA_AUTO && flag_sensor_cachorro) {
        codigoEvento = SENSOR_CACHORRO; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
    else if (estado == ESPERA && !flag_sensor_cachorro && !alarme_peso_cte) {
        codigoEvento = SENSOR_N_CACHORRO; 
        xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
        if( xStatus != pdPASS )
          Serial.println("Erro ao enviar evento para fila");
    }
  }
}

void setup() {
  Serial.begin(115200); 
  rtc_clk_cpu_freq_set(RTC_CPU_FREQ_80M);

  //inicia os componentes
  conecta_wifi();
  bal_init();
  pres_init();
  myservo.attach(SERVOPIN);
  
  xQueue = xQueueCreate(5, sizeof(int));
  if(xQueue != NULL)
  {
    xTaskCreate(taskMaqEstados,"taskMaqEstados", 100, NULL, 2, &xTaskMaqEstados);
    xTaskCreate(taskObterEvento,"taskObterEvento", 100, NULL, 1, &xTaskObterEvento);
    vTaskStartScheduler();
  }
  else
  {
    /* Não criou a fila */
  }

}


void loop() {
  //Feito por RTOS
}
